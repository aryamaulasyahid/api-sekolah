<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class SiswaController extends Controller
{
  
    public function index()
    {
        $siswa = DB::select('select * from siswa');
        return $siswa;
    }
    public function show($id) {
        $siswa = DB::select('select * from siswa where id_siswa = ?',[$id]);
        return $siswa;
    }

    public function create(Request $request){
        $nisn = $request->input('nisn');
        $nama = $request->input('nama');
        $jk = $request->input('jk');
        $kelas = $request->input('kelas');
        $ttl = $request->input('ttl');
        $alamat = $request->input('alamat');
        
        $data=array('nisn'=>$nisn,"nama"=>$nama,"jk"=>$jk,"kelas"=>$kelas, "ttl"=>$ttl, "alamat"=>$alamat,  );
        DB::table('siswa')->insert($data);
        return "Data berhasil disimpan";
        }

    public function update(Request $request,$id) {
        $nisn = $request->input('nisn');
        $nama = $request->input('nama');
        $jk = $request->input('jk');
        $kelas = $request->input('kelas');
        $ttl = $request->input('ttl');
        $alamat = $request->input('alamat');
        DB::update('update siswa set nisn = ?,nama=?,jk=?,kelas=?,ttl=?,alamat=? where id_siswa = ?',[$nisn,$nama,$jk,$kelas,$ttl,$alamat,$id]);
        return "Data berhasil diupdate";
    }

    public function delete($id) {
        DB::delete('delete from siswa where id_siswa = ?',[$id]);
        return "Data berhasil dihapus";
    }
}
