<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;

class MahasiswaController extends Controller
{
  
    public function index()
    {
        return Mahasiswa::all();
    }

    public function create(request $request)
    {
        $mahasiswa = new Mahasiswa;
        $mahasiswa->NIM = $request->NIM;
        $mahasiswa->nama_mhs = $request->nama_mhs;
        $mahasiswa->angkatan = $request->angkatan;
        $mahasiswa->alamat = $request->alamat;
        $mahasiswa->save();

        return "Data berhasil disimpan !";
    }

    public function update(request $request, $id)
    {
        $NIM = $request->NIM;
        $nama_mhs = $request->nama_mhs;
        $angkatan = $request->angkatan;
        $alamat = $request->alamat;

        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->NIM = $NIM;
        $mahasiswa->nama_mhs = $nama_mhs;
        $mahasiswa->angkatan = $angkatan;
        $mahasiswa->alamat = $alamat;
        $mahasiswa->save();

        return "Data berhasil diupdate !";
    }

    public function delete($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->delete();

        return "data berhasil dihapus";
    }
}
